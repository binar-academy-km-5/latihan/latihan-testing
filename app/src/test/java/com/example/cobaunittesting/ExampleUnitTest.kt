package com.example.cobaunittesting

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
	fun init(){
		// Inisialisasi dummy Data
		val dummy = Utils.dummyList
		Utils.initializeList()

		// Tambahkan Data pada arrayList
		val testList = Utils.bungaList
		Utils.addList("Mawar")
		Utils.addList("Daisy")
	}

	@Test
	fun arrayListNotEmpty(){
		init()
		val testList = Utils.bungaList

		// Buktikan bahwa arrayList tidak kosong
		assert(!testList.isNullOrEmpty())
	}

	@Test
	fun arrayListNotNull(){
		init()
		val testList = Utils.bungaList

		// Buktikan bahwa arrayList tidak null
		assertNotNull(testList)
	}

	@Test
	fun arrayListIsSame(){
		init()
		val testList = Utils.bungaList
		val dummy = Utils.dummyList

		// Buktikan bahwa arrayListnya sama dengan Dummy
		assertEquals(dummy,testList)

	}
}