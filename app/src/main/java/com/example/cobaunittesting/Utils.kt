package com.example.cobaunittesting

object Utils {
	val dummyList: ArrayList<String> = ArrayList()
	val bungaList: ArrayList<String> = ArrayList()

	fun addList(flower: String): ArrayList<String>{
		bungaList.add(flower)
		return bungaList
	}

	fun initializeList(){
		dummyList.add("Mawar")
		dummyList.add("Daisy")
	}
}